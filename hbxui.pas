unit HbxUi;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Menus, ActnList, StdActns, ComCtrls, ExtCtrls, Buttons,
  LCLType, DateUtils, synaser;

type

  { TAppForm }

  TAppForm = class(TForm)
    DnKey: TBitBtn;
    EastKey: TBitBtn;
    EnterKey: TButton;
    GotoKey: TButton;
    Key0: TButton;
    Key1: TButton;
    Key2: TButton;
    Key3: TButton;
    Key4: TButton;
    Key5: TButton;
    Key6: TButton;
    Key7: TButton;
    Key8: TButton;
    Key9: TButton;
    FLowKeypadPanel: TPanel;
    ModeKey: TButton;
    NorthKey: TBitBtn;
    FKeypadPanel: TPanel;
    SouthKey: TBitBtn;
    UpKey: TBitBtn;
    FileConnectAction: TAction;
    FileExitAction: TFileExit;
    FileSetupAction: TAction;
    AppActionList: TActionList;
    AppImageList: TImageList;
    AppMainMenu: TMainMenu;
    FileItem: TMenuItem;
    DisplayMemo: TMemo;
    HelpSubmenu: TMenuItem;
    FileConnectItem: TMenuItem;
    MenuItem10: TMenuItem;
    FileExitItem: TMenuItem;
    MenuItem12: TMenuItem;
    MenuItem13: TMenuItem;
    MenuItem15: TMenuItem;
    MenuItem16: TMenuItem;
    MenuItem17: TMenuItem;
    MenuItem18: TMenuItem;
    MenuItem19: TMenuItem;
    FileSetupItem: TMenuItem;
    AppStatusBar: TStatusBar;
    Timer1: TTimer;
    AppToolBar: TToolBar;
    ConnectToolButton: TToolButton;
    SetupToolButton: TToolButton;
    WestKey: TBitBtn;
    procedure FileSetupActionUpdate(Sender: TObject);
    procedure KeypadMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure KeypadMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FileConnectActionExecute(Sender: TObject);
    procedure FileConnectActionUpdate(Sender: TObject);
    procedure FileSetupActionExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    function  GetConnected: Boolean;
    procedure SetConnected(connect: Boolean);
  private
    FSerialPortName: String;
    FBlockSerial   : TBlockSerial;
    FPressedKey    : TObject;
    FHeldKeyTimeout: TDateTime;
  public
    property  Connected: Boolean read GetConnected write SetConnected;
  published
    property  SerialPortName: String read FSerialPortName write FSerialPortName;
  end;

var
  AppForm: TAppForm;

implementation

uses
  SetupDlg;

{$R *.lfm}

{ TAppForm }

procedure TAppForm.FileSetupActionExecute(Sender: TObject);
begin
  Application.CreateForm(TSetupForm, SetupForm);
  SetupForm.SerialPortDevice := SerialPortName;
  if SetupForm.ShowModal = mrOk then
  begin
    SerialPortName := SetupForm.SerialPortDevice;
    AppStatusBar.Panels[0].Text:= SerialPortName;
  end;
  FreeAndNil(SetupForm);
end;

procedure TAppForm.FileSetupActionUpdate(Sender: TObject);
begin
  FileSetupAction.Enabled := not Connected;
end;

procedure TAppForm.FormCreate(Sender: TObject);
begin
  FBlockSerial := TBlockSerial.Create;
  FBlockSerial.RaiseExcept := True;
  AppStatusBar.Panels[0].Text:= '';
  AppStatusBar.Panels[1].Text:= 'Disconnected';
end;

procedure TAppForm.FormDestroy(Sender: TObject);
begin
  if Connected then
    Connected := False;
  FreeAndNil(FBlockSerial);
end;

procedure TAppForm.Timer1Timer(Sender: TObject);
var
  buffer: String;
  line  : String;
  curpos: TPoint;
  s     : TStringList;
  rawpos: Byte;
begin
  { :ED# : commands the Autostar to send to the PC whatever is
    currently on the display screen.
    You will receive 32 characters, preceded by a bit-encoded status byte.
    That byte has the current cursor position (1 bit for Y, 4 for X)
  }
  s := TStringList.Create;
  try
    try
      FBlockSerial.SendString(':ED#');
      buffer := FBlockSerial.RecvTerminated(500, '#');
      rawpos := Byte(buffer.Chars[0]);
      curpos.x := rawpos and $0f;
      curpos.y := (rawpos div 16) and $01;
      line := buffer.Substring(1, 16);
      if line.StartsWith('Telescope') then
      begin
        line := buffer.Substring(17, 16);
        buffer := buffer.Substring(17, Length(buffer) - 17);
      end;
      s.Append(line.Replace(#223, 'º'));
      line := buffer.Substring(17, 16);
      s.Append(line.Replace(#223, 'º'));
      DisplayMemo.Lines.BeginUpdate;
      DisplayMemo.Lines.Assign(s);
      DisplayMemo.Lines.EndUpdate;
      DisplayMemo.CaretPos := curpos;
    except
      on e: ESynaSerError do
        FBlockSerial.Purge;
    end;
  finally
    s.Free;
  end;
end;

procedure TAppForm.KeypadMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
{$push}{$warn 5024 off}
begin
  if not FileConnectAction.Checked then
    Exit;

  if Button <> mbLeft then
    Exit;

  FPressedKey := Sender;
  FHeldKeyTimeout := IncSecond(Time, 2);
end;

{$pop}

procedure TAppForm.KeypadMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
{$push}{$warn 5024 off}
var
  Code: String;
begin
  if not FileConnectAction.Checked then
    Exit;

  if Button <> mbLeft then
    Exit;

  Assert(FPressedKey = Sender);

  Code := '';
  with Sender as TButtonControl do
    case Caption of
      '0':     Code := '48';
      '1':     Code := '49';
      '2':     Code := '50';
      '3':     Code := '51';
      '4':     Code := '52';
      '5':     Code := '53';
      '6':     Code := '54';
      '7':     Code := '55';
      '8':     Code := '56';
      '9':     Code := '57';
      'Dn':    Code := '85';
      'E':     Code := '69';
      'N':     Code := '78';
      'S':     Code := '83';
      'Up':    Code := '68';
      'W':     Code := '87';
      '?':     Code := '63';
    else
      if CompareTime(Time, FHeldKeyTimeout) < 0 then // Short keypress
        case Caption of
          'ENTER': Code := '13';
          'GOTO':  Code := '71';
          'MODE':  Code := '9';
        end
      else  // Long keypress
        case Caption of
          'ENTER': Code := '10';
          'MODE':  Code := '11';
          'GOTO':  Code := '25';
        end;
    end;

  Assert(Code <> '');
  try
    FBlockSerial.SendString(':EK' + Code + '#');
  except
    on e: ESynaSerError do
      FBlockSerial.Purge;
  end;
end;
{$pop}

procedure TAppForm.FileConnectActionExecute(Sender: TObject);
begin
  Connected :=  FileConnectAction.Checked;
end;

procedure TAppForm.FileConnectActionUpdate(Sender: TObject);
begin
  if FSerialPortName.IsEmpty then
  begin
    FileConnectAction.Checked := False;
    FileConnectAction.Enabled := False;
    Exit;
  end;

  FileConnectAction.Enabled := True;
end;

function TAppForm.GetConnected: Boolean;
begin
  Result := FBlockSerial.InstanceActive;
end;

procedure TAppForm.SetConnected(connect: Boolean);
begin
  if connect then  // Connect
  begin
    Assert(not FBlockSerial.InstanceActive);
    try
      // Connect & configure serial port
      FBlockSerial.Connect(FSerialPortName);
      FBlockSerial.Config(9600, 8, 'N', SB1, false, false);
      // Update GUI
      FileConnectAction.Checked := True;
      FileConnectAction.ImageIndex := 3;
      AppStatusBar.Panels[1].Text := 'Connected';
      FKeypadPanel.Enabled := True;
      // Enable periodic handbox screen retrieval
      Timer1.Enabled := True;
    except
      on e: ESynaSerError do
      begin
        FBlockSerial.CloseSocket;
        Application.MessageBox(
          PChar(e.ErrorMessage),
          PChar('Could not connect to "' + FSerialPortName + '"'),
          MB_OK + MB_ICONHAND
        );
      end;
    end;
  end
  else  // Disconnect
  begin
    Assert(FBlockSerial.InstanceActive);
    // Disable periodic screen retrieving
    Timer1.Enabled := False;
    // Update GUI
    FKeypadPanel.Enabled := False;
    AppStatusBar.Panels[1].Text := 'Disconnected';
    FileConnectAction.ImageIndex := 2;
    FileConnectAction.Checked := False;
    // Disconnect from serial port
    FBlockSerial.CloseSocket;
  end;
end;

end.

