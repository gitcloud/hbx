unit SetupDlg;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ButtonPanel, ExtCtrls;

type

  { TSetupDlg }

  { TSetupForm }

  TSetupForm = class(TForm)
    SetupFormPanel: TPanel;
    SetupFormButtonPanel: TButtonPanel;
    SerialPortComboBox: TComboBox;
    SerialPortCBLabel: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure SerialPortComboBoxGetItems(Sender: TObject);
  private
    function  GetSerialPortDevice: String;
    procedure SetSerialPortDevice(const aDevice: String);
    procedure UpdateAvailableSerialPorts;
  public

  published
    property SerialPortDevice: String
      read  GetSerialPortDevice
      write SetSerialPortDevice;
  end;

var
  SetupForm: TSetupForm;

implementation

uses
  SynaSer;

{$R *.lfm}

{ TSetupForm }

procedure TSetupForm.SerialPortComboBoxGetItems(Sender: TObject);
begin
  UpdateAvailableSerialPorts;
end;

procedure TSetupForm.FormCreate(Sender: TObject);
begin
  with SerialPortComboBox.Items do
  begin
    StrictDelimiter := True;
    Delimiter := ',';
  end;
end;

function TSetupForm.GetSerialPortDevice: String;
begin
  Result := SerialPortComboBox.Text;
end;

procedure TSetupForm.SetSerialPortDevice(const aDevice: String);
begin
  UpdateAvailableSerialPorts;
  with SerialPortComboBox do
    ItemIndex := Items.IndexOf(aDevice);
end;

procedure TSetupForm.UpdateAvailableSerialPorts;
begin
  SerialPortComboBox.Items.DelimitedText := GetSerialPortNames;
end;

end.

